"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var index_1 = require("./dashboard/index");
var index_2 = require("./login/index");
var index_3 = require("./home/index");
var index_4 = require("./register/index");
var appRoutes = [
    { path: 'dashboard', component: index_1.DashboardComponent },
    { path: 'home', component: index_3.HomeComponent },
    { path: 'login', component: index_2.LoginComponent },
    { path: 'register', component: index_4.RegisterComponent },
    // otherwise redirect to home
    { path: '**', redirectTo: 'dashboard' }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map